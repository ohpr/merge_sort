def mergeSort(list_):
    
    if len(list_) <= 1: # The algorithm asks if the length of the list is unequal (equal or larger) to "1", as we want to decompose the original list into ultimately individual items.
    
        return list_ # In case len(list_) == 1, then the algorithm will return the one single item in the list - explanation below...
    
    middle = len(list_) // 2 # If the list doesn't consist of only one item yet, then we here determine the middle point of the list in order to break it down below.
    
    left = mergeSort(list_[:middle]) # We define a variable "left", which calls the function again, mergeSort, this time with only the "left part" of the list we are breaking down.
    # Here, since we call the function again, the whole process of mergeSort starts all over - the function will ask if len(list_) = 1, if not, it will keep breaking down the list.
    # When finally length == 1, a value will be returned and left will be assigned a value - when this happens, the "recursiveness" begins. Left gets a value...
    # ... and we continue where we left on the last iteration of the previous mergeSort run - now we determine "right" - it follows that right will also be a one-single item list.
    
    right = mergeSort(list_[middle:])
    
    # When the value for right is returned - we then jump to the next phase of the function, which is "return reduce(left, right)" - this is another function...
    # ... i.e. another function is called here.
    
    return reduce(left, right)


def reduce(left, right):
    
    result = [] # Firstly, we create an empty list to store the results of the function.
    left_index = 0 # We assign a variable that will count the first item in "left" and "right" in order to compare these below.
    right_index = 0
    
    while True: # While any of the following statements is True, continue the loop.
        
        if left[left_index] < right[right_index]: # On the first function call, the if-statement asks: "is left[0] < right [0]? - if so, then...
        # In other words, it's asking whether the left number is smaller than the right number and if that's the case, the following happens:
            
            result.append(left[left_index]) # If that's the case, the left number will be assigned into the emtpy list - we then skip the "else" part that follows.
            left_index += 1 # We increase left[0] by 1 => left[1] in order to continue comparing numbers inside the "left" list with those in the "right" list.
        # If it's not the case that left[0] < right[0], then we skip the first "if" and go into the "else" statement:
        else:
            
            result.append(right[right_index]) # If we end up here, it's because left > right - hence, we assign the right value into the empty list "result = []".
            right_index += 1 # We increase right[0] by 1 => right[1] in order to keep comparing the first number in "left", with the following numbers in "right".
            
        if left_index == len(left): # This will be "True" when we have compared all numbers inside "left" with those in "right" - and when all the numbers in "left" are smaller than those in "right".
        # If this criteria is met, it follows that this function will increase the list by the "right" value. The logic of this part is that we can compare all numbers of a list until left_index...
        # ... equals the length of the list.
            
            result.extend(right[right_index:]) # The "result" list is "extended" by adding "right". "right" is already sorted.
            
            break # We don't need to do anything else, hence we break in the case this if-statement is run.
        
        if right_index == len(right): # This if-statement is run in the case right < left - as the above if statement would be False in this case.
            
            result.extend(left[left_index:]) # Same thing as above happens - but here the list would be extended by adding the "left" item. "left" is already sorted.
            
            break
        
    return result

