import time

n = 1000

results_wc = []

while n <= 512000:
    
    trial_wc = list(reversed(range(0, n + 1)))
    
    print("For " + str(n) + " items, the processing time in seconds is:")
    
    start = time.time()
    
    mergeSort(trial_wc)
    
    end = time.time()
    
    total_time = end - start
    
    results_wc.append(total_time)
    
    print(total_time)
    
    n = n * 2

